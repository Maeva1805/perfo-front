import React, { useState } from 'react';
import './index.scss'

export const SlowPage = () => {

    return (
        <div className="cmdContainer">
            <h1>Slow page</h1>
            
            <p>This page is slow...</p>
        </div>
    )
}