import React, { useState } from 'react';
import './index.scss';

export const Login = (props) => {
    const { setLoggedIn } = props;

    const [email, setEmail] = useState("");
    const [pwd, setPwd] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoggedIn(true);
    }

    return (
        <div className="loginPage">
            <form onSubmit={handleSubmit}>
                <input
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                />

                <input
                type="password"
                value={pwd}
                onChange={(e) => setPwd(e.target.value)}
                />

                <button type="submit">Log in</button>
            </form>
        </div>
    );
}
