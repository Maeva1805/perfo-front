import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Switch, NavLink } from "react-router-dom";

// COMPONENTS
import { NavMenu } from './components/navMenu/NavMenu';

// PAGES
import { Login } from './pages/login/Login';
import { Home } from './pages/home/Home';
import { Data } from './pages/data/Data';
import { SlowPage } from './pages/slowPage/SlowPage';

export const App = () => {
  const [loggedIn, setLoggedIn] = useState(localStorage.getItem("token") !== null);

  return (
    <div id="appContainer">
      <Router>
          {/*loggedIn &&
            <Login setLoggedIn={setLoggedIn}/>
          */}
          <NavMenu setLoggedIn={setLoggedIn}/>

          <div id="mainContainer">
            <Switch>

              {/* Home */}
              <Route exact path='/'>
                  <Home/>
              </Route>

              {/* Data */}
              <Route exact path='/data'>
                  <Data/>
              </Route>

              {/* Slow */}
              <Route exact path='/slow'>
                  <SlowPage/>
              </Route>

            </Switch>
          </div>
      </Router>
    </div>
  );
}

export default App;
