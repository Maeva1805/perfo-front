import React from 'react';
import { NavLink } from "react-router-dom";
import './index.scss';

export const NavMenu = (props) => {
    const { setLoggedIn } = props;

    const logout = () => {
        localStorage.removeItem('token');
        setLoggedIn(false);
    }

    return (
        <nav id='navMenu'>
            <ul>
                <li>
                    <NavLink to="/" exact>
                        <i className="fas fa-home"></i>
                        <span>Home</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/data">
                        <i className="fas fa-database"></i>
                        <span>Data</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/slow">
                        <i className="fas fa-spinner"></i>
                        <span>Slow page</span>
                    </NavLink>
                </li>
                <li onClick={logout} className="logout">
                    <i className="fas fa-sign-out-alt"></i>
                    <span>Log out</span>
                </li>
            </ul>
        </nav>
    );
}
